provider "aws" {
  profile = "default"
  region  = "eu-west-3"
  # access_key = var.access_key
  # secret_key = var.secret_key
}

terraform {
  backend "s3" {
    bucket = "tfjordan"
    key    = "worktf/terraform.tfstate"
    region = "eu-west-3"
  }
}
