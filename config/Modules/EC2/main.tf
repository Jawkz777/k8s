resource "aws_instance" "prometheus" {
  for_each = toset (var.ec2_name)
  ami           = data.aws_ami.aws_linux.id
  instance_type = var.instance_type
  key_name = var.key_name
  
  tags = {
    Name = each.value
  }
}
