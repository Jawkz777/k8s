variable "instance_type" {
  type = string
}

variable "key_name" {
  type = string
}

variable "ec2_name" {
  type = list(string)
  default = ["Jordan-K8S-Master", "Jordan-K8S-Worker"]
}